package modulo2;

public class Ejercicio2 {
	public static void main(String[] args) {
		byte      bmin = -128;
	    byte      bmax = 127;
//		reemplazar el 0 por el valor que corresponda en todos los caso
	    short     smin = -32768;
	    short     smax = 32767;
	    int       imax = -2147483648;
	    int       imin = 2147483647;
	    long      lmin = -9223372036854775808L;
	    long      lmax = 9223372036854775807L;
	    System.out.println("tipo\tminimo\tmaximo");
		System.out.println("....\t......\t......");
		System.out.println("\nbyte\t" + bmin + "\t" + bmax);
		System.out.println("....\t......\t......");
		System.out.println("\nshort\t" + smin + "\t" + smax);
		System.out.println("....\t......\t......");
		System.out.println("\nint\t" + imin + "\t" + imax);
		System.out.println("....\t......\t......");
		System.out.println("\nlong\t" + lmin + "\t" + lmax);
		System.out.println("....\t......\t......");
		System.out.println("La formula general para calcular el valor maximo es 2^(nde bits)-1 y el valor minimo 2^(nde bits)");

	}


}
