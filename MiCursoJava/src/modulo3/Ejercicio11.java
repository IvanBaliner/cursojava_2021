package modulo3;

import java.util.Scanner;

public class Ejercicio11 {
	public static void main(String[] args) {
		char l;
		Scanner scan = new Scanner(System.in);
		System.out.println(" Ingresar una letra: A");
		l = scan.next().charAt(0);
		if (l=='a' || l=='e' || l=='i' || l=='o' || l=='u') {
			System.out.println(l+" Es una vocal ");
		}
		else {
			System.out.println(l+" Es una consonante ");
		}

	}

}
