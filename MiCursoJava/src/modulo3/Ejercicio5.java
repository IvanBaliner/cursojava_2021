package modulo3;

import java.util.Scanner;

public class Ejercicio5 {
	public static void main(String[] args) {
		int pos;
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la posicion del participante(n):");
		pos = scan.nextInt();
		
		if (pos>=1) {
			if (pos==1) {
				System.out.println("Obtiene medalla de oro");
			}
			else if (pos==2) {
				System.out.println("Obtiene medalla de plata");
			}
			else if (pos==3) {
				System.out.println("Obtiene medalla de bronce");
			}
			else {
				System.out.println("Suerte para la proxima");
			}
		}

	}


}
