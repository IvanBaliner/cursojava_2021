package modulo3;

import java.util.Scanner;

public class Ejercicio13 {
	public static void main(String[] args) {
		String mes;
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un mes (con mayuscula al inicio)");
		mes = scan.next();
		
		switch(mes) {
		case("Septiembre"):
			System.out.println(mes+ " Tiene 30 dias ");
			break;
		case("Abril"):
			System.out.println(mes+ " Tiene 30 dias ");
			break;
		case("Noviembre"):
			System.out.println(mes+ " Tiene 30 dias ");
			break;
		case("Junio"):
			System.out.println(mes+ " Tiene 30 dias ");
			break;
		case("Febrero"):
			System.out.println(mes+ " Tiene 28 dias ");
			break;
		default:
			System.out.println(mes+ " Tiene 31 dias ");
			
		}

	}


}
