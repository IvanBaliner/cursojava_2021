package modulo3;

import java.util.Scanner;

public class Ejercicio12 {
	public static void main(String[] args) {
		int n;
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un numero dentro de las primeras tres docenas de numeros naturales:");
		n = scan.nextInt();
		
		if(n>=1 && n<=12) {
			System.out.println("El numero esta en la primera docena");
		}
		else if (n>12 && n<=24) {
			System.out.println("El numero esta en la segunda docena");
		}
		else if (n>24 && n<=36) {
			System.out.println("El numero esta en la tecera docena");
		}
		else {
			System.out.println("El numero "+n+" esta fuera de rango");

				}

		}
}
