package modulo3;

import java.util.Scanner;

public class Ejercicio17 {
	public static void main(String[] args) {
		int valor;
		int sumap=0;
		Scanner scan = new Scanner(System.in);
		System.out.println(" Ingrese un valor: ");
		valor = scan.nextInt();
		
		for(int i=1;i<=10;i++) {
			int r=valor*i;
			System.out.println(valor+" x "+i+" = "+r);
			if(r%2==0) {
				sumap = sumap+r; 
			}
		}
		System.out.println(" La suma de los resultados pares es: "+sumap);

	}


}
