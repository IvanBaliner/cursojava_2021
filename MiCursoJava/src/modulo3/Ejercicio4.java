package modulo3;

import java.util.Scanner;

public class Ejercicio4 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese una categoria A, B o C");
        String categoria = scan.nextLine();

        if (categoria.equals("A"))
            System.out.println("Hijo");
        else if (categoria.equals("B"))
            System.out.println("Padres");
        else if (categoria.equals("C"))
            System.out.println("Abuelos");
        else
            System.out.println("No ingreso una categoria valida");
	}


}
