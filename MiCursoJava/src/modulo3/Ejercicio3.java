package modulo3;

import java.util.Scanner;

public class Ejercicio3{
	public static void main(String[] args) {
		String mes;
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un mes (con mayuscula)");
		 mes = scan.next();
		if(mes.equals("Noviembre") || mes.equals("Abril") || mes.equals("Junio") || mes.equals("Septiembre")) {
			System.out.println(mes+" tiene 30 dias");
		}
		else if(mes.equals("Febrero")){
			System.out.println(mes+" tiene 28 dias");
		}
		else {
			System.out.println(mes+" tiene 31 dias");
		}
	}


}
