package modulo3;

import java.util.Scanner;

public class Ejercicio9 {
	public static void main(String[] args) {
		int cmp1, cmp2;
		Scanner scan = new Scanner(System.in);
		System.out.println("(0 = Piedra, 1 = Papel, 2 = Tijera");
		System.out.println("Ingresar la eleccion del competidor 1:");
		cmp1 = scan.nextInt();
		System.out.println("Ingresar la eleccion del competidor 2:");
		cmp2 = scan.nextInt();
		System.out.println("");
		
		if (cmp1!=cmp2) {
			if (cmp1==0 && cmp2==1) {
				System.out.println("Gana el competidor 2");
			}
			if (cmp1==0 && cmp2==2) {
				System.out.println("Gana el competidor 1");
			}
			if (cmp1==1 && cmp2==0) {
				System.out.println("Gana el competidor 1");
			}
			if (cmp1==1 && cmp2==2) {
				System.out.println("Gana el competidor 2");
			}
			if (cmp1==2 && cmp2==0) {
				System.out.println("Gana el competidor 2");
			}
			if (cmp1==2 && cmp2==1) {
				System.out.println("Gana el competidor 1");
			}
		}
		else {
		System.out.println("Empate");
		}

	}


}
