package modulo3;

import java.util.Scanner;

public class Ejercicio21 {
	public static void main(String[] args) {
		char cat;
		int  sldbase, antig;
		double sldneto = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println(" Categoria(a,b,c):");
		cat = scan.next().charAt(0);
		System.out.println(" Antiguedad:");
		antig = scan.nextInt();
		System.out.println(" Sueldo base:");
		sldbase = scan.nextInt();
		
		if (cat=='a') 
			sldneto=sldbase+1000;
		
		if (cat=='b') 
			sldneto=sldbase+2000;
		
		if (cat=='c') 
			sldneto=sldbase+3000;
				
		if (antig>=1 && antig<=5) {
			sldneto= sldneto+(sldneto*0.05);
		}
		else if (antig>5 && antig<10) {
			sldneto=(float)(sldbase+(sldbase*0.1));
		}
		else {
			sldneto=(float)(sldbase+(sldbase*0.3));
		}
		System.out.println("El sueldo neto es: $"+sldneto);

	}


}
