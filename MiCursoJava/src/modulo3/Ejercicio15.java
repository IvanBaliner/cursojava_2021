package modulo3;

import java.util.Scanner;

public class Ejercicio15 {
	public static void main(String[] args) {
		char auto;
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la categoria del auto(en minuscula):");
		auto = scan.next().charAt(0);
		
		switch(auto) {
		case'a':
			System.out.println(" El auto tiene 4 ruedas y un motor ");
			break;
		case'b':
			System.out.println(" El auto tiene 4 ruedas, un motor, cierre centralizado y aire ");
			break;
		case'c':
			System.out.println(" El auto tiene 4 ruedas, un motor, cierre centralizado, aire y airbag. ");
			break;
		default:
			System.out.println(" Categoria incorrecta ");
			
		}


	}


}
