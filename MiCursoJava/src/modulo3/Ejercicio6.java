package modulo3;

import java.util.Scanner;

public class Ejercicio6 {
	public static void main(String[] args) {
		int curso;
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un curso entre 0 y 12:");
		curso = scan.nextInt();
		
		if (curso==0) {
			System.out.println(" Jardin de infantes ");
		}
		else if (curso>=1 && curso<=6) {
			System.out.println(" Primaria ");
		}
		else if (curso>=7 && curso<=12) {
			System.out.println(" Secundaria ");
		}

	}


}
