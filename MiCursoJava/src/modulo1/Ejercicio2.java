package modulo1;

public class Ejercicio2 {
	public static void main(String[] args) {
		System.out.println("Tecla de escape\t\tsignificado\n");
		System.out.println("\\n\t\t\tsingnifica nueva linea");
		System.out.println("\\t\t\t\tsingnifica un tab de espacio");
		System.out.println("\\\"\t\t\tes para poner \" (comillas dobles) dentro del texto por ejemplo");
		System.out.println("\t\t\t\"Belencita\"");
		System.out.println("\\\\\t\t\tse utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\");
		System.out.println("\\\'\t\t\tse utiliza para las \'(comilla simple) para escribir por ejemplo \'Princesita\'");
		
	}


}
